import scrapy
from scrapy.http import FormRequest

class BlogSpider(scrapy.Spider):
    name = 'blogspider'
    start_urls = ['https://blog.scrapinghub.com']

    def parse(self, response):
        for title in response.css('h2.entry-title'):
            yield {'title': title.css('a ::text').extract_first()}

        next_page = response.css('div.prev-post > a ::attr(href)').extract_first()
        if next_page:
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

            


class e17(scrapy.Spider):
    name = 'e17'
    #start_urls = ['http://www.consiglionazionaleforense.it/web/cnf/cerca-avvocato']

#    def start_request(self):
 #       hxs = HtmlXPathSelector(response)
  #      goodForm = hxs.select('//form[@name="searchAvvocati"]/@action')
    def start_requests(self):
        return [FormRequest(url="http://www.consiglionazionaleforense.it/web/cnf/cerca-avvocato",
                            formdata={'citta':'Lucca'},
                            callback=self.resultList)]
    def resultList(self, response):
        hxs = scrapy.Selector(response)
        rows = hxs.xpath('//div[@class="row-fluid"]/div[@class="single-result box0"]//div[@class="span4"]/form/@action').extract()
        for row in rows:
            yield {'link': row}
